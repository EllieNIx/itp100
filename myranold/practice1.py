def add_time(hours, minutes, addhours, addminutes):
        """
           >>> add_time(5, 54, 2, 2)
           '7:56'
           >>> add_time(6, 13, 1, 1)
           '7:14'
        """
        addminutes = addminutes + minutes
        addhours = addhours + hours +(addminutes //60)
        if addminutes < 10:
            return f'{addhours}:0{addminutes}'
        return f'{addhours}:{addminutes}'

if __name__ == '__main__':
        import doctest
        doctest.testmod()
