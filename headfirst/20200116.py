#today i worked on pages 1-13
#testing paragraphs 
movies = ["The Holy Grail", 1975, "The Life of Brian", 1979, "The Meaning of Life", 1983]
print(movies[0])
cast = ["Cleese", 'Palin', 'Jones', "Idle"]
print(cast)

print(len(cast))

cast.append("Gilliam")
print(cast)

cast.pop()

print(cast)

cast.extend(["Gilliam", "Chapman"])
print(cast)

#exersize
#pt 1
movies1 = ["The Holy Grail", "The Life of Brian", "The Meaning of Life"]
movies1.insert(1, 1975)
movies1.insert(3, 1979)
movies1.insert(5, 1983)

print(movies1)
#pt2
movies2 = ["The Holy Grail", 1975, "The Life of Brian", 1979, "The Meaning of Life", 1983]
print(movies2)
#pt2 seems like the better idea

