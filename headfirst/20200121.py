
#today in class I worked on page 14-22
fav_movies = ["The Holy Grail", "The Life of Brian"]

print(fav_movies[0])
print(fav_movies[1])

for each_flick in fav_movies:
    print(each_flick)

count = 0 
while count < len(movies):
    print(movies[count])
    count = count+1

for each_item in movies:
    print(each_item)
movies = [
        "The Holy Grail", 1975, "Terry Jones & Terry Gilliam", 91,
            ["Graham Chapman", 
                ["Michael Palin", "John Cleese", "Terry Gilliam", "Eric Idle", "Terry Jones"]]]
print(movies[4][1][3])

print(movies)

for each_item in movies:
    print(each_item)
#note: if [some condition holds]:
#         [the "true" suite]
#      else:
#           [the "false" suite]

names = ['Michael', 'Terry']
isinstance(names, list)

num_names = len(names)
isinstance(num_names, list)

#exercise

for each_item in movies:
    if isinstance(each_item, list):
        for nested_item in each_item:
            print(nested_item)
    else:
        print(each_item)


#used homework time to finish and understand 20 practice problems on gmetrix to prepare for
#retake on the 29th
