from gasp import *

scale = 10

begin_graphics()

player_x = 100
player_y = 100
c = Circle((player_x, player_y), 5, filled=True)

robot_x = 400
robot_y = 100
r = Circle((robot_x, robot_y), 30, filled=False)
    
robot_x2 = 320
robot_y2 = 100
r2 = Circle((robot_x2, robot_y2), 25, filled=False)

robot_x3 = 480
robot_y3 = 100
r3 = Circle((robot_x3, robot_y3), 25, filled=False)

intro_text = Text("Aight so u are the black dot", (20, 400), size=40)
text2 = Text("-just like dont die dummy", (150, 350), size=25) 
text3 = Text("-use wasd keys its not hard", (150, 320), size=25)
text4 = Text("-Click 'Enter' to start", (150, 290), size=25)
box = Box((10, 270), 600, 200, filled=False)

finished = False
moves = 0

while not finished:
    key = update_when('key_pressed')
    if key == 'enter':
        remove_from_screen(intro_text)
        remove_from_screen(text2)
        remove_from_screen(text3)
        remove_from_screen(box)
        remove_from_screen(text4)
    if key == 'w':
        player_y += scale
    elif key == 's':
        player_y -= scale
    elif key == 'd':
        player_x += scale
    elif key == 'a':
        player_x -= scale
    move_to(c, (player_x, player_y))
    
    pin = random_between(0,1)

    if pin == 1 and player_y != robot_y:
        if robot_y < player_y:
            robot_y += scale * 2
        elif robot_y > player_y:
            robot_y -= scale * 2
    elif pin == 0 and player_x != robot_x:
        if robot_x < player_x:
            robot_x += scale * 2
        elif robot_x > player_x:
            robot_x -= scale * 2
    move_to(r, (robot_x, robot_y))
    
    if pin == 1 and player_y != robot_y2:
        if robot_y2 < player_y:
            robot_y2 += scale
        elif robot_y2 > player_y:
            robot_y2 -= scale
    elif pin == 0 and player_x != robot_x2:
        if robot_x2 < player_x:
            robot_x2 += scale
        elif robot_x2 > player_x:
            robot_x2 -= scale
    move_to(r2, (robot_x2, robot_y2))
    
    if pin == 1 and player_y != robot_y3:
        if robot_y3 < player_y:
            robot_y3 += scale
        elif robot_y3 > player_y:
            robot_y3 -= scale
    elif pin == 0 and player_x != robot_x3:
        if robot_x3 < player_x:
            robot_x3 += scale
        elif robot_x3 > player_x:
            robot_x3 -= scale
    move_to(r3, (robot_x3, robot_y3))
    
    player_x = player_x % 640
    player_y %= 480
    robot_x %= 640
    robot_y %= 480
    moves += 1
 
    player_x = player_x % 640
    player_y %= 480
    robot_x2 %= 640
    robot_y2 %= 480
    moves += 1
     
    player_x = player_x % 640
    player_y %= 480
    robot_x3 %= 640
    robot_y3 %= 480
    moves += 1
    
    if robot_x == player_x and robot_y == player_y:
        finished == True
    if robot_x2 == player_x and robot_y2 == player_y:
        finished == True    
    if robot_x3 == player_x and robot_y3 == player_y:
        finished == True


end_text = Text(f"HA loser You only got {moves} moves. Press 'x' to exit.", (20, 300), size=25)

while True:
    key = update_when('key_pressed')
    if key == 'x':
        break

end_graphics()

