def base64decode(four_chars):
	    """
	      >>> base64decode('STOP')
	      b'I3\\x8f'
	    """
	    digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
	    ints = [digits.index(ch) for ch in four_chars]
	    b1 = (ints[0] << 2) | ((ints[1] & 48) >> 4)
	    b2 = (ints[1] & 15) << 4 | ints[2] >> 2
            b3 = ((ints[2] & 3 )<< 6) | (ints[3])

	    return bytes([b1, b2, b3]))
