def decode4chars(s):
    """
    Return 4 base64 encoded characters to 3 bytes from which they orginated.
    Handle special endings == and = where only 1 or 2 bytes are returned
    repectively.

      >>> decode4chars('STOP')
      b'I3\\x8f'
      >>> decode4chars('Wivm')
      b'Z+\xe6'
      >>> decode4chars('////')
      b'\xff\xff\xff'
      >>> decode4chars('VA==')
      b'T'
      >>> decode4chars('VGU=')
      b'Te'
      >>> decode4chars('AA==')
      b'\\x00'
      >>> decode4chars('AAA=')
      b'\\x00\\x00'
      >>> decode4chars('/w==')
      b'\\xff'
      >>> decode4chars('//8=')
      b'\\xff\\xff'
    """
    digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

    if len(s) != 4 or not all([ch in digits + '=' for ch in s]):
        raise ValueError(f'{s} is not a value base64 encoded string')

    # Compute first byte
    int1 = digits.index(s[0])
    int2 = digits.index(s[1])
    b1 = (int1 << 2) | ((int2 & 48) >> 4)

    # Handle single byte return case
    if s[2:] == '==':
        return bytes([b1])

    int3 = digits.index(s[2])
    b2 = (int2 & 15) << 4 | int3 >> 2

    # Handle 2 byte return case
    if s[3:] == '=':
        return bytes([b1, b2])

    int4 = digits.index(s[3])
    b3 = (int3 & 3) << 6 | int4

    return bytes([b1, b2, b3])


def base64encode(f):
    """
    """


def base64decode(f):
    """
    """


if __name__ == '__main__':
    import doctest
    doctest.testmod()

