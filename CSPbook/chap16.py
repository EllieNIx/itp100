#stage 1-----
print("stage 1")

name = "Mark"
start = "My name is "
combined = start + name
print(len(combined))
print(combined)
print( name * 3)

#stage 2-----
print("stage 2")

a_string ="wack"
b_string = "bow"
c_string = (a_string + b_string) * 2
print(len(c_string))

#stage 3-----
print("stage 3")

my_first_list = [12,"ape",13]
print(len(my_first_list))
print(my_first_list * 3)
my_second_list = my_first_list + [321.4]
print(my_second_list)

#stage 4-----
print("stage 4")

def name_procedure(name):
    start = "My name is "
    combined = start + (name * 2)
    print(combined)
    print(len(combined))


name_procedure("John")

#stage 5-----
print("stage 5")

def item_lister(items):
    items[0] = "First item"
    items[1] = items[0]
    items[2] = items[2] + 1
    print(items)

item_lister([2, 4, 6, 8])

#stage 6-----
print("stage 6")

a_list = [99, 100, 74, 63, 100, 100]
sum = 0

for num in a_list:
    sum = sum + num
average = sum / len(a_list)

print(a_list)
print(average)

#stage 7
print("stage 7")

source = ["This", "is", "a", "list"]
so_far = []
for index in range(0,len(source)):
    so_far = [source[index]] + so_far
print(so_far)

#stage 8
print("stage 8")

items = ["hi", 2, 3, 4]
items[0] = items[0] * 2
items[1] = items[2] - 3
items[2] = items[1]
print(items)

#stage 9
print("stage 9")

# setup the source list
source = ["This","is", "a","list"]

# Set the accumulator to the empty list
so_far = []

# Loop through all the items in the source list
for index in range(0, len(source)):

    # Add the current item in the source and print the current items in so_far
    so_far = [source[index]] + so_far
    print(so_far)

#stage 10
print("stage 10")

# setup the source list
source = ["This","is","a","list"]

# Set the accumulator to the empty list
so_far = []

# Loop through all the items in the source list
for index in range(0,len(source)):

    # Add a list with the current item from source to so_far
    so_far =  [source[index]] + so_far +  [source[index]]
print(so_far)


#stage 11
print("stage 11")

numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
even_list = []
for index in range(0,len(numbers),2):
    even_list = even_list + [numbers[index]]
print(even_list)

#stage 12
print("stage 12")

# loop though every other index
for index in range(0, 51, 5):
    print(index)

#stage 13
print("stage 13")

for index in range(60, -1, -1):
    print(index)

#stage 15
print("stage 15")

for index in range(1, 10, 2):
    print(index)

#stage 16
print("stage 16")

list = [1, 2, 3, 4]
so_far=[]
for num in list:
    so_far = num + 5
    print(so_far)


#stage 17
print("stage 17")

list = [1, 2, 3, 4]
sum = 0
for num in list:
      sum = sum + num

print(sum)

#stage 18
print("stage 18")

list = [1, 2, 3, -4]
sum = 0
for num in list:
    sum = sum + (abs(num))
print(sum)


#stage 19
print("stage 19")

for index in range(5, 0, -2):
    print(index)

#stage 20
print("stage 20")

finished = sum(range(1, 20, 2))
print(finished)

print("Sorry it's so late, I hope I can still get some credit")
